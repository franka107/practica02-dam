import React from 'react';
import {View, Text, StatusBar, Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import MainScreen from './screens/MainScreen';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import QueryScreen from './screens/QueryScreen';
import MapScreen from './screens/MapScreen';
import Transference from './screens/Transference';
import LoadingScreen from './screens/LoadingScreen';

console.ignoredYellowBox = ['Setting a timer'];
const Stack = createStackNavigator();

const Drawer = createDrawerNavigator();

const DrawerScreens = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen
        name="Query"
        options={{
          title: 'Consultas',
        }}
        component={QueryScreen}
      />
      <Drawer.Screen
        name="Maps"
        component={MapScreen}
        options={{
          title: 'Ubicanos',
          headerShown: true, //Con esto vuelve invisible el header
          headerTransparent: false, //Con esto vuelves transparente
          headerStyle: {
            backgroundColor: '#6200ee', //background del header
          },
          headerTitleStyle: {
            fontWeight: 'bold',
            color: 'white',
          },
          headerTintColor: 'white', //color de la flechita
        }}
      />
      <Drawer.Screen
        name="Transference"
        options={{
          title: 'Transferencias',
        }}
        component={Transference}
      />
    </Drawer.Navigator>
  );
};

const App = (props) => {
  //useEffect(() => {
  //function _getRealTimeData() {}
  //function _getNormalData() {}
  //});
  //});
  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor="#6200ed" barStyle="light-content" />
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: '#6200ed',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}>
          <Stack.Screen 
            name="Splash" 
            component={LoadingScreen}
            options={{headerShown:false}}
          />
          <Stack.Screen name="Home" component={MainScreen} options={{
              title: 'BCPuma',}}/>
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            /* options={{
              title: 'Login',
              headerStyle: {
                backgroundColor: '#6200ed',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}   */
          />
          <Stack.Screen name="Register" component={RegisterScreen} />
          <Stack.Screen
            name="Maps"
            component={MapScreen}
            options={{
              title: 'Ubicanos',
              headerShown: true, //Con esto vuelve invisible el header
              headerTransparent: false, //Con esto vuelves transparente
              headerStyle: {
                backgroundColor: '#6200ee', //background del header
              },
              headerTitleStyle: {
                fontWeight: 'bold',
                color: 'white',
              },
              headerTintColor: 'white', //color de la flechita
            }}
          />
          <Stack.Screen
            name="Drawer"
            options={{
              title: 'BCPuma',
            }}
            component={DrawerScreens}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
};

export default App;

//import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
//import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
//import {NavigationContainer} from '@react-navigation/native';
//
//import MapView from './screens/MapScreen';
//
//const Tab = createMaterialBottomTabNavigator();
//
//function MyTabs() {
//  return (
//    <NavigationContainer>
//      <Tab.Navigator
//        initialRouteName="Home"
//        tabBarOptions={{
//          activeTintColor: '#e91e63',
//        }}
//        barStyle={{
//          backgroundColor: '#6200ee',
//        }}>
//        <Tab.Screen
//          name="Feed"
//          component={MapView}
//          options={{
//            tabBarLabel: 'Home',
//            tabBarIcon: ({color}) => (
//              <MaterialCommunityIcons name="home" color={color} size={26} />
//            ),
//          }}
//        />
//      </Tab.Navigator>
//    </NavigationContainer>
//  );
//}
//
