import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  StatusBar,
  TextInput,
  Animated,
  Dimensions,
  TouchableOpacity,
  Alert,
  YellowBox,
} from 'react-native';
import {TypingAnimation} from 'react-native-typing-animation';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-community/async-storage';

import firebase from '../plugins/firebase.config';
import 'firebase/firestore';

const db = firebase.firestore();
db.settings({experimentalForceLongPolling: true});

export default class App extends React.Component {
  constructor(props) {
    super(props);
    YellowBox.ignoreWarnings(['Setting a timer']);
    this.state = {
      typing_email: false,
      typing_password: false,
      animation_login: new Animated.Value(width - 40),
      enable: true,
      email: '',
      password: '',
    };
  }

  validator = (navigation) => {
    if (this.state.email == '') {
      Alert.alert(
        'Oops, Something Happened',
        'Enter your e-mial, you cannot leave this field blank',
      );
    } else if (this.state.password == '') {
      Alert.alert(
        'Oops, Something Happened',
        'Enter your password, you cannot leave this field blank',
      );
    } else {
      this.verifyLogin();
    }
  };

  verifyLogin() {
    if (this.state.email.trim && this.state.password) {
      db.collection('users')
        .where('email', '==', this.state.email)
        .where('password', '==', this.state.password)
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach(async (doc) => {
            try {
              if (doc.data()) {
                const jsonValue = JSON.stringify(doc.data());
                await AsyncStorage.setItem('userId', doc.id);
                await AsyncStorage.setItem('userData', jsonValue);
                this.props.navigation.navigate('Drawer');
              }
            } catch (e) {
              console.log(e);
            }
          });
        });
    }
  }

  _foucus(value) {
    if (value == 'email') {
      this.setState({
        typing_email: true,
        typing_password: false,
      });
    } else {
      this.setState({
        typing_email: false,
        typing_password: true,
      });
    }
  }

  _typing() {
    return (
      <TypingAnimation
        dotColor="#8ed16f"
        style={{marginRight: 25, marginTop: 25}}
      />
    );
  }

  _animation() {
    Animated.timing(this.state.animation_login, {
      toValue: 40,
      duration: 250,
    }).start();

    setTimeout(() => {
      this.setState({
        enable: false,
        typing_email: false,
        typing_password: false,
      });
    }, 5000);
  }

  render() {
    const width = this.state.animation_login;
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
        <View style={styles.header}>
          <ImageBackground
            source={require('../assets/header.png')}
            style={styles.imageBackground}>
            <Text
              style={{
                color: 'white',
                fontWeight: 'bold',
                fontSize: 30,
              }}>
              Welcome Back
            </Text>
            <Text
              style={{
                color: '#d6d6e3',
              }}>
              Sign in to continute
            </Text>
          </ImageBackground>
        </View>
        <View style={styles.footer}>
          <Text
            style={[
              styles.title,
              {
                marginTop: 50,
              },
            ]}>
            E-mail
          </Text>
          <View style={styles.action}>
            <TextInput
              placeholder="Your email.."
              style={styles.textInput}
              onFocus={() => this._foucus('email')}
              value={this.state.email}
              onChangeText={(value) => this.setState({email: value})}
            />
            {this.state.typing_email ? this._typing() : null}
          </View>

          <Text
            style={[
              styles.title,
              {
                marginTop: 20,
              },
            ]}>
            Password
          </Text>
          <View style={styles.action}>
            <TextInput
              secureTextEntry
              placeholder="Your password.."
              style={styles.textInput}
              onFocus={() => this._foucus('password')}
              value={this.state.password}
              onChangeText={(value) => this.setState({password: value})}
            />
            {this.state.typing_password ? this._typing() : null}
          </View>

          <TouchableOpacity
            onPress={() => this.validator(this.props.navigation)}>
            <View style={styles.button_container}>
              <Animated.View
                style={
                  this.state.enable
                    ? [styles.animation, {width}]
                    : [styles.animation1, {width}]
                }>
                {this.state.enable ? (
                  <Text style={styles.textLogin}>Login</Text>
                ) : (
                  <Animatable.View animation="bounceIn" delay={50}>
                    <FontAwesome name="check" color="white" size={20} />
                  </Animatable.View>
                )}
              </Animated.View>
            </View>
          </TouchableOpacity>

          <View style={styles.signUp}>
            <Text style={{color: 'black'}}>New user?</Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.replace('Register')}>
              <Text style={{color: '#64934e'}}> Sign up?</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const width = Dimensions.get('screen').width;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  header: {
    flex: 1,
  },
  footer: {
    flex: 2,
    padding: 20,
    backgroundColor: 'white',
    opacity: 0.7
  },
  imageBackground: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  title: {
    color: 'black',
    fontWeight: 'bold',
  },
  action: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
  },
  textInput: {
    flex: 1,
    marginTop: 5,
    paddingBottom: 5,
    color: 'gray',
  },
  button_container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  animation: {
    backgroundColor: '#6200ed',
    paddingVertical: 10,
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  animation1: {
    backgroundColor: '#6200ed',
    paddingVertical: 10,
    marginTop: 30,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textLogin: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
  },
  signUp: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20,
  },
});
