/* import React, {useEffect, useState} from 'react'; */
import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  Alert,
  TouchableOpacity,
  PermissionsAndroid,
} from 'react-native';
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  Callout,
  AnimatedRegion,
  Animated,
} from 'react-native-maps';

import firebase from '../plugins/firebase.config';
import 'firebase/firestore';

const db = firebase.firestore();
db.settings({experimentalForceLongPolling: true});

export default class MapScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prueba: [],
      region: {
        latitude: -16.409046,
        longitude: -71.537453,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    };
    this.onRegionChange.bind(this);
  }

  componentDidMount() {
    db.collection('places')
      .get()
      .then((docSnapshot) => {
        console.log(`Received doc snapshot: ${docSnapshot.docs}`);

        docSnapshot.forEach((doc) => {
          console.log(
            'AQUI ESTA LOS DATOS OBTENIDOS',
            doc.id,
            '=> ',
            doc.data(),
            doc.data().name,
            doc.data().longitude,
          );
          this.setState({
            prueba: [...this.state.prueba, doc.data()],
          });
        });
      })
      .catch((error) => {
        console.log(
          'There has been a problem with your fetch operation: ' +
            error.message,
        );
      });
  }

  onRegionChange(region) {
    this.setState({region});
  }
  render() {
    const {region} = this.state;
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          initialRegion={region}>
          {this.state.prueba.map((item, index) => {
            return (
              <Marker
                coordinate={this.state.coordinate}
                key={index}
                coordinate={{
                  latitude: parseFloat(item.latitude),
                  longitude: parseFloat(item.longitude),
                }}
                image={require('../assets/bank.png')}>
                <Callout>
                  <View
                    style={{
                      backgroundColor: '#FFFFFF',
                      borderRadius: 5,
                      width: 150,
                    }}>
                    <Text style={{fontWeight: 'bold', alignSelf: 'center'}}>
                      {item.name}
                    </Text>
                    <Text numberOfLines={3} style={{textAlign: 'justify'}}>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry.
                    </Text>
                  </View>
                </Callout>
              </Marker>
            );
          })}
        </MapView>
        {/* <Button
          title="Insertar"
          onPress={() => {
            console.log('Hola');
            db.collection('places')
              .doc()
              .add({
                name: 'Sucursal Centro',
                latitude: '-16.408933',
                longitude: '-71.543397',
              })
              .then((res) => {
                console.log('Listo');
              });
          }}></Button> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  map: {
    flex: 1,
    /*  width: 50,
    height: 100, */
  },
});
