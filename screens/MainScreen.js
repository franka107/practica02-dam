import React, { useState, useEffect, Component } from 'react';
import { Animated, StyleSheet, Image, View, Text, SafeAreaView, ScrollView, StatusBar } from 'react-native';
import { Button } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
//import { useNavigation } from '@react-navigation/native';
import HomeImage from '../assets/home.png'


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      LogoAnime: new Animated.Value(0),
      LogoText: new Animated.Value(0),
      loadingSpinner: true,
    };
  }
  
  animateWidth = () => {
    const {LogoAnime, LogoText} = this.state;
    Animated.loop(
      Animated.spring(LogoAnime, {
        toValue: 1,
        tension: 10,
        friction: 1,
        duration: 1000,
      }).start()
    )
  }
  componentDidMount(){
    this.animateWidth() 
  }

  render(){
    return (
      <View style={{flex: 1}}>
        <ScrollView>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <StatusBar
              backgroundColor="#6200ed"
              barStyle="light-content"
            />
            <Animated.View style={{
              opacity: this.state.LogoAnime,
              top: this.state.LogoAnime.interpolate({
                inputRange: [0, 1],
                outputRange: [80, 0],
              }),
              paddingTop: '30%',
            }}>
              <Image source={HomeImage} style={{width: 400,  height: 300,}}/>
            </Animated.View>
            <View style={{ paddingTop: '10%', paddingBottom: '25%', flexDirection: "row"}}>
              <View style={styles.btnLogin}>
                <Button 
                    mode="contained"
                    onPress={() => this.props.navigation.navigate('Login')}
                    icon="account-circle"
                    color="#b0df9a"
                  >Go to Login</Button>
              </View>
              <View style={styles.btnLogin}>
                <Button 
                  style={styles.btnLogin} 
                  mode="contained"
                  onPress={() => this.props.navigation.navigate('Maps')}
                  color="#b0df9a"
                >Go to Maps</Button>
              </View>    
            </View>
          </View>
        </ScrollView>
        <View style={styles.footerView}>
          <Text style={styles.footerText}>"If you let fear stop you</Text>
          <Text style={styles.footerText}>You can never move forward"</Text>
        </View>
      </View>
    );
  }
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  btnLogin:{
    paddingRight: '3%',
    paddingLeft: '3%'
  },
  footerText: {
    fontStyle: 'italic',
    fontWeight: 'bold'
  },
  footerView: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '1%',
    marginBottom: '1%',
  }
});

