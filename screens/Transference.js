import React, {useEffect, useState} from 'react';
import { FAB, Portal, Provider } from 'react-native-paper';

import {
  View,
  Text,
  YellowBox,
  StyleSheet,
  TouchableOpacity,
  Alert,
  ImageBackground,
  Image,
  ScrollView
} from 'react-native';
import firebase from '../plugins/firebase.config';
import AsyncStorage from '@react-native-community/async-storage';
import {TextInput, Button} from 'react-native-paper';

YellowBox.ignoreWarnings(['Setting a timer']);
const db = firebase.firestore();

const Transference = () => {
  const [accounts, setAccounts] = useState([]);
  const [account, setAccount] = useState('');
  const [account2, setAccount2] = useState('');
  const [amount, setAmount] = useState(0.0);

  async function getAccountbyUser() {
    const userId = await AsyncStorage.getItem('userId');
    await db
      .collection('accounts')
      .where('userId', '==', userId)
      .get()
      .then((querySnapshot) => {
        let aux = [];
        querySnapshot.forEach((doc) => {
          aux = [...aux, {id: doc.id, ...doc.data()}];
        });
        setAccounts(aux);
      });
    console.log(accounts);
  }

  async function getAccount(account) {
    let cuenta;
    await db
      .collection('accounts')
      .where('number', '==', account)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          console.log(doc.data());
          cuenta = doc.id;
        });
      })
      .catch((err) => console.log(err));
    return cuenta;
  }
  async function getData(id) {
    let num;
    await db
      .collection('accounts')
      .doc(id)
      .get()
      .then((respDoc) => {
        console.log(`queryByTitulo=> ${respDoc.data().quantity}`);
        num = respDoc.data().quantity;
      });
    return num;
  }
  async function updateOriginAndDestiny(idOrigin, idDestiny, amount) {
    db.collection('accounts')
      .doc(idDestiny)
      .update({
        quantity: parseFloat(await getData(idDestiny)) + parseFloat(amount),
      });
    db.collection('accounts')
      .doc(idOrigin)
      .update({
        quantity: parseFloat(await getData(idOrigin)) - parseFloat(amount),
      });
    Alert.alert('Resultado de operacion', 'La operacion se realizo con Exito', [
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ]);
  }
  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1,backgroundColor: 'white'}}>      
      <View style={styles.container}>
        <View style={styles.header}>
          <ImageBackground
            source={require("../assets/headerRegister.png")}
            style={styles.imageBackground}
          >
            <Image source={require("../assets/transference.png")} style={styles.image}/>
          </ImageBackground>
          <Text style={{
              paddingTop: '5%',
              color:'black',
              fontWeight:'bold',
              fontSize:30,
            }}>Transferences</Text>
        </View>
        <View style={styles.footer}>
          <TextInput
            mode={'outlined'}
            style={styles.textInput}
            value={account}
            onChangeText={(text) => setAccount(text)}
            label="Root account"></TextInput>
          <TextInput
            mode={'outlined'}
            style={styles.textInput}
            value={account2}
            onChangeText={(text) => setAccount2(text)}
            label="Destination account"></TextInput>
          <TextInput
            mode={'outlined'}
            style={styles.textInput}
            value={amount}
            onChangeText={(text) => setAmount(text)}
            label="Quantity to send"></TextInput>
          <Button 
            style={{marginTop:20}} 
            mode="contained"
            onPress={async () =>
              updateOriginAndDestiny(
                await getAccount(account),
                await getAccount(account2),
                amount,
              )
            }
            color="#6200ed"
          >Transfer</Button>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  textInput: {
    //borderRadius: 5,
    marginTop: 10,
    marginBottom: 10
  },
  container: {
    backgroundColor:'white',
    justifyContent:'center',
    marginBottom: 200,
  },
  header: {
    flex:1,
    alignItems: 'center'
  },
  imageBackground:{
    alignItems:'center',
    width:"100%",
    height:'80%'
  },
  image:{
    paddingTop: '50%',
    width:"60%",
    height:'60%'
  },
  footer: {
    flex:2,
    position: 'relative',
    padding:20,
    marginTop: '1%',
  },
});

export default Transference;
