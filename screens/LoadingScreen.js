import React from 'react';
import { View, Text, StyleSheet, Animated, Image } from 'react-native';
import Logo from '../assets/credit-card.png';
import LogoTexto from '../assets/bcp.png';
import { useNavigation } from '@react-navigation/native';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      LogoAnime: new Animated.Value(0),
      LogoText: new Animated.Value(0),
      loadingSpinner: false,
    };
  }

  componentDidMount(){
    const {LogoAnime, LogoText} = this.state;
    const switchAuth = () => {
      this.props.navigation.replace('Home')
    };
    Animated.parallel([
      Animated.spring(LogoAnime, {
        toValue: 1,
        tension: 10,
        friction: 2,
        duration: 1000,
      }).start(),

      Animated.timing(LogoText, {
        toValue: 1,
        duration: 3000,
      })
    ]).start(() => {
      this.setState({
        loadingSpinner: true,
      });
      setTimeout(switchAuth, 1200)
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Animated.View style={{
          opacity: this.state.LogoAnime,
          top: this.state.LogoAnime.interpolate({
            inputRange: [0, 1],
            outputRange: [80, 0],
          }),
          paddingBottom: '10%',
        }}>
          <Image source={Logo} style={{width: 250,  height: 180,}}/>
        </Animated.View>
        <Animated.View style={{
          opacity: this.state.LogoText,
        }}>
          <Image source={LogoTexto} style={{width: 250,  height: 40}}/>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#6200ed",
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoText: {
    color: "#FFFFFF",
    fontFamily: "GoogleSans-Bold",
    fontSize: 30,
    marginTop: 29.1,
    fontWeight: '300',
  }
});