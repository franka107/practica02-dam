import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  StatusBar,
  TextInput,
  Animated,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Alert,
} from "react-native";
import { TypingAnimation } from 'react-native-typing-animation';
import { Button } from 'react-native-paper';
import { CommonActions } from '@react-navigation/native';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import * as Animatable from 'react-native-animatable';

import firebase from '../plugins/firebase.config';
import 'firebase/firestore';

const db = firebase.firestore();
db.settings({experimentalForceLongPolling: true});

export default class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
      typing_email: false,
      typing_password: false,
      typing_confirm: false,
      typing_name: false,
      typing_lastname: false,
      typing_dni: false,
      animation_login : new Animated.Value(width-40),
      enable:true,
      name: '',
      lastname: '',
      dni: '',
      email: '',
      password: '',
      confirm: '',
    }
  }

  createUser() {
    db.collection('users').doc().set({
      name: this.state.name,
      lastName: this.state.lastname,
      email: this.state.email,
      dni: this.state.dni,
      password: this.state.confirm,
    }).then( ()=> {
      console.log('Añadido exitosamente');
      Alert.alert('Exito amiguito', 'Registro Exitoso')
    }).catch( error => {
      Alert.alert('Que mal amiguito', 'Registro fallido')
      console.log( 'Error:' , error);
    });
  }

  validator = (navigation) => {
    if(this.state.name == ''){
      Alert.alert('Oops, Something Happened', 'Enter your name, you cannot leave this field blank')
    } else if(this.state.lastname == '') {
      Alert.alert('Oops, Something Happened', 'Enter your last name, you cannot leave this field blank')
    } else if(this.state.dni == '') {
      Alert.alert('Oops, Something Happened', 'Enter your DNI, you cannot leave this field blank')
    } else if(this.state.email == '') {
      Alert.alert('Oops, Something Happened', 'Enter your email, you cannot leave this field blank')
    } else if(this.state.password == '') {
      Alert.alert('Oops, Something Happened', 'Enter your password, you cannot leave this field blank')
    } else if(this.state.confirm != this.state.password) {
      Alert.alert('Oops, Something Happened', 'Passwords do not match')
    } else {
      this.createUser();
      this._animation();
      navigation.replace('Login');
    }
  };

  _foucus(value){
    if(value=="name"){
      this.setState({
        typing_email: false,
        typing_password: false,
        typing_dni: false,
        typing_name: true,
        typing_lastname: false,
        typing_confirm: false,
      })
    }
    if(value=="lastname"){
      this.setState({
        typing_email: false,
        typing_password: false,
        typing_dni: false,
        typing_name: false,
        typing_lastname: true,
        typing_confirm: false,
      })
    }
    if(value=="dni"){
      this.setState({
        typing_email: false,
        typing_password: false,
        typing_dni: true,
        typing_name: false,
        typing_lastname: false,
        typing_confirm: false,
      })
    }
    if(value=="email"){
      this.setState({
        typing_email: true,
        typing_password: false,
        typing_dni: false,
        typing_name: false,
        typing_lastname: false,
        typing_confirm: false,
      })
    }
    if(value=="password"){
      this.setState({
        typing_email: false,
        typing_password: true,
        typing_dni: false,
        typing_name: false,
        typing_lastname: false,
        typing_confirm: false,
      })
    }
    if(value=="confirm"){
      this.setState({
        typing_email: false,
        typing_password: false,
        typing_dni: false,
        typing_name: false,
        typing_lastname: false,
        typing_confirm: true,
      })
    }
  }

  _typing(){
    return(
      <TypingAnimation 
        dotColor="#6200ed"
        style={{marginRight:25,marginTop:25,}}
      />
    )
  }

  _animation(){
    Animated.timing(
      this.state.animation_login,
      {
        toValue: 40,
        duration: 250
      }
    ).start();

    setTimeout(() => {
      this.setState({
        enable:false,
        typing_email: false,
        typing_password: false,
        typing_dni: false,
        typing_name: false,
        typing_lastname: false,
        typing_confirm: false,
      })
    }, 150)
  }

  render(){
    const width = this.state.animation_login;
    return(
      <ScrollView contentContainerStyle={{ flexGrow: 1, backgroundColor: '#fff'}}>      
        <View style={styles.container}>
          <StatusBar barStyle="light-content" />
            <View style={styles.header}>
                <ImageBackground
                  source={require("../assets/headerRegister.png")}
                  style={styles.imageBackground}
                >
                  <Text style={{
                    color:'white',
                    fontWeight:'bold',
                    fontSize:30,
                    marginTop: '15%',
                  }}>Sign Up</Text>
                  <Text style={{
                    color:'yellow'
                  }}>Sign up to continue</Text>

                </ImageBackground>
            </View>
            <View style={styles.footer}>
                  <Text style={styles.title}>Name</Text>
                  <View style={styles.action}>
                      <TextInput 
                        placeholder="Your name.."
                        style={styles.textInput}
                        onFocus={()=>this._foucus("name")}
                        value={this.state.name}
                        onChangeText={ value => this.setState({name: value})}
                      />
                      {this.state.typing_name ?
                        this._typing()
                      : null}
                  </View>

                  <Text style={[styles.title,{
                    marginTop:20
                  }]}>Last name</Text>
                  <View style={styles.action}>
                      <TextInput 
                        placeholder="Your last name.."
                        style={styles.textInput}
                        onFocus={()=>this._foucus("lastname")}
                        value={this.state.lastname}
                        onChangeText={ value => this.setState({lastname: value})}
                      />
                      {this.state.typing_lastname ?
                        this._typing()
                      : null}
                  </View>

                  <Text style={[styles.title,{
                    marginTop:20
                  }]}>DNI</Text>
                  <View style={styles.action}>
                      <TextInput 
                        placeholder="Your dni.."
                        style={styles.textInput}
                        onFocus={()=>this._foucus("dni")}
                        value={this.state.dni}
                        onChangeText={ value => this.setState({dni: value})}
                      />
                      {this.state.typing_dni ?
                        this._typing()
                      : null}
                  </View>

                  <Text style={[styles.title,{
                    marginTop:20
                  }]}>E-mail</Text>
                  <View style={styles.action}>
                      <TextInput 
                        placeholder="Your e-mail.."
                        style={styles.textInput}
                        onFocus={()=>this._foucus("email")}
                        value={this.state.email}
                        onChangeText={ value => this.setState({email: value})}
                      />
                      {this.state.typing_email ?
                        this._typing()
                      : null}
                  </View>
                  

                  <Text style={[styles.title,{
                    marginTop:20
                  }]}>Password</Text>
                  <View style={styles.action}>
                      <TextInput 
                        secureTextEntry
                        placeholder="Your password.."
                        style={styles.textInput}
                        onFocus={()=>this._foucus("password")}
                        value={this.state.password}
                        onChangeText={ value => this.setState({password: value})}
                      />
                      {this.state.typing_password ?
                        this._typing()
                      : null}
                  </View>

                  <Text style={[styles.title,{
                    marginTop:20
                  }]}>Confirm Password</Text>
                  <View style={styles.action}>
                      <TextInput 
                        secureTextEntry
                        placeholder="Confirm your password.."
                        style={styles.textInput}
                        onFocus={()=>this._foucus("confirm")}
                        value={this.state.confirm}
                        onChangeText={ value => this.setState({confirm: value})}
                      />
                      {this.state.typing_confirm ?
                        this._typing()
                      : null}
                  </View>
                  <TouchableOpacity
                  onPress={()=>this.validator(this.props.navigation)}>
                    <View style={styles.button_container}>
                          <Animated.View style={this.state.enable ? [styles.animation,{width}] : [styles.animation1,{width}]}>
                            {this.state.enable ?
                              <Text style={styles.textLogin}>Register</Text>
                              :
                              <Animatable.View
                              animation="bounceIn"
                              delay={50}>
                                <FontAwesome 
                                  name="check"
                                  color="white"
                                  size={20}
                                />
                              </Animatable.View>
                            }
                          </Animated.View >
                    </View>
                  </TouchableOpacity>
            </View>
        </View>
      </ScrollView>
    )
  }
}

const width = Dimensions.get("screen").width;

var styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:'white',
    justifyContent:'center',
    marginBottom: 200,
  },
  header: {
    flex:1,
  },
  footer: {
    flex:2,
    padding:20,
    marginTop: '10%',
  },
  imageBackground:{
    flex:1,
    alignItems:'center',
    width:"100%",
    height:'120%'
  },
  title: {
    color:'black',
    fontWeight:'bold'
  },
  action: {
    flexDirection:'row',
    borderBottomWidth:1,
    borderBottomColor:'#f2f2f2'
  },
  textInput: {
    flex:1,
    marginTop:5,
    paddingBottom:5,
    color:'gray'
  },
  button_container: {
    marginTop: '3%',
    alignItems: 'center',
    justifyContent:'center'
  },
  animation: {
    backgroundColor:'#6200ed',
    paddingVertical:10,
    marginTop:30,
    justifyContent:'center',
    alignItems:'center'
  },
  animation1: {
    backgroundColor:'#6200ed',
    paddingVertical:10,
    marginTop:30,
    borderRadius:100,
    justifyContent:'center',
    alignItems:'center'
  },
  textLogin: {
    color:'white',
    fontWeight:'bold',
    fontSize:18
  },
  signUp: {
    flexDirection:'row',
    justifyContent:'center',
    marginTop:20
  }
});