/* eslint-disable prettier/prettier */
import React, {useEffect, useState} from 'react';
import {View, Text, YellowBox} from 'react-native';
import {List, Checkbox, Appbar, FAB, Portal, Provider} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from '../plugins/firebase.config';
import 'firebase/firestore';
import {create} from 'react-test-renderer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

YellowBox.ignoreWarnings(['Setting a timer']);

const db = firebase.firestore();
//db.settings({experimentalForceLongPolling: true});

const QueryScreen = ({route, navigation}) => {
  const [isExpanded, setIsExpanded] = useState(true);
  const [accounts, setAccounts] = useState([]);
  const [cards, setCards] = useState([]);
  const [state, setState] = React.useState({ open: false });

  const onStateChange = ({ open }) => setState({ open });

  const { open } = state;

  useEffect(() => {
    function getUsers() {
      db.collection('users')
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            console.warn(doc.id, '=> ', doc.data());
          });
        })
        .catch((error) => {
          console.log('Error getting document', error);
        });
    }
    function createUserAccount() {
      db.collection('users')
        .doc('w9sBWaWXDB9P4aaSDHW0')
        .collection('accounts')
        .doc()
        .set({
          type: 'Corriente',
          badge: 'Soles',
          number: '843141-123-23',
        })
        .then(() => {
          console.log('Añadido exitosamente');
        })
        .catch((error) => {
          console.log('Error:', error);
        });
    }
    function createUserCards() {
      db.collection('users')
        .doc('w9sBWaWXDB9P4aaSDHW0')
        .collection('cards')
        .doc()
        .set({
          type: 'Clasica',
          badge: 'Dolares',
          enterprise: 'Visa',
          number: '6575-3345-2342-2097',
        })
        .then(() => {
          console.log('Añadido exitosamente');
        })
        .catch((error) => {
          console.log('Error:', error);
        });
    }
    async function getUserAccounts() {
      try {
        const userId = await AsyncStorage.getItem('userId');
        await db
          .collection('accounts')
          .where('userId', '==', userId)
          .get()
          .then((querySnapshot) => {
            let aux = [];
            querySnapshot.forEach((doc) => {
              aux = [...aux, {id: doc.id, ...doc.data()}];
            });
            setAccounts(aux);
          });
      } catch (e) {
        console.log(e);
      }
    }
    async function getUserCards() {
      try {
        const userId = await AsyncStorage.getItem('userId');
        await db
          .collection('users')
          .doc(userId)
          .collection('cards')
          .get()
          .then((querySnapshot) => {
            let aux = [];
            querySnapshot.forEach((doc) => {
              aux = [...aux, {id: doc.id, ...doc.data()}];
            });
            setCards(aux);
          });
      } catch (e) {
        console.log(e);
      }
    }
    getUserAccounts();
    getUserCards();
  }, []);

  return (
    <View style={{flex:1}}>
      <List.Section title="Consultas">
        <List.Accordion
          title="Cuentas Bancarias"
          left={(props) => (
            <List.Icon {...props} icon="format-list-numbered" />
          )}>
          {accounts.map((account) => (
            <List.Item
              title={account.type}
              description={account.number}
              right={() => (
                <Text
                  style={{
                    alignSelf: 'center',
                    paddingRight: 20,
                    fontWeight: 'bold',
                  }}>
                  S/.{account.quantity}
                </Text>
              )}
              left={(props) => (
                <Text
                  style={{
                    color: 'white',
                    textAlign: 'center',
                    textAlignVertical: 'center',
                    borderRadius: 5,
                    backgroundColor: 'tomato',
                    width: 70,
                  }}>
                  {account.badge}
                </Text>
              )}
            />
          ))}
        </List.Accordion>
        <List.Accordion
          title="Tarjetas"
          left={(props) => <List.Icon {...props} icon="card-bulleted" />}>
          {cards.map((card) => (
            <List.Item
              title={card.type}
              description={card.number}
              left={(props) => (
                <Text
                  style={{
                    color: 'white',
                    textAlign: 'center',
                    textAlignVertical: 'center',
                    borderRadius: 5,
                    backgroundColor: '#fdba03',
                    width: 70,
                  }}>
                  {card.enterprise}
                </Text>
              )}
            />
          ))}
        </List.Accordion>
      </List.Section>
      <Provider>
        <Portal>
          <FAB.Group
            open={open}
            icon={open ? 'card-bulleted' : 'plus'}
            color={open ? 'white' : '#6200ed'}
            fabStyle={open ? {backgroundColor:'black'} : {backgroundColor:'white'}}
            actions={[
              {
                icon: 'card',
                label: 'Make a Tranference',
                onPress: () => navigation.navigate('Transference'),
              },
            ]}
            onStateChange={onStateChange}
            onPress={() => {
              if (open) {
                // do something if the speed dial is open
              }
            }}
          />
        </Portal>
      </Provider>
    </View>
  );
};

export default QueryScreen;
