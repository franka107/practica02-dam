/* eslint-disable prettier/prettier */
import * as firebase from 'firebase';

 var firebaseConfig = {
    apiKey: 'AIzaSyCHCrsB135C88Bx4XYay8uj7wNo1IIG614',
    authDomain: 'project02-3cd37.firebaseapp.com',
    databaseURL: 'https://project02-3cd37.firebaseio.com',
    projectId: 'project02-3cd37',
    storageBucket: 'project02-3cd37.appspot.com',
    messagingSenderId: '600307829944',
    appId: '1:600307829944:web:a417b6d6924f454d34384d',
    measurementId: 'G-VDXGEYMBB9',
  };

firebase.initializeApp(firebaseConfig);
export default firebase;


